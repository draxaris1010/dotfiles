#!/usr/bin/busybox sh
set -f
[ -d $XDG_RUNTIME_DIR/awesomewm ] || mkdir -p $XDG_RUNTIME_DIR/awesomewm
O=$(getopt -uo i:t: -- "$@")
set -- $O
while true;do
    case "$1" in
        -i)
            INTERFACE="$2";shift 2;;
        -t)
            TIMEOUT="$2";shift 2;;
        --)
            shift;break;;
        *)
            echo "Parameter $1 not recognized!";exit 1;;
    esac
done

set +f
while true
do
    cat /sys/class/net/$INTERFACE/statistics/*_bytes > $XDG_RUNTIME_DIR/awesomewm/net
    sleep $TIMEOUT
done
