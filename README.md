[![forthebadge](https://forthebadge.com/images/badges/powered-by-electricity.svg)](https://forthebadge.com)

[![forthebadge](https://forthebadge.com/images/badges/compatibility-club-penguin.svg)](https://forthebadge.com)

# dotfiles
These are my dotfiles.

You can follow [this guide from Atlassian](https://www.atlassian.com/git/tutorials/dotfiles) to install the dotfiles.
