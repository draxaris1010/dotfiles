"---Start Vundle config---"
set nocompatible	" be iMproved
filetype off

" set runtime path
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle
Plugin 'VundleVim/Vundle.vim'

" My plugins installed
Plugin 'vim-airline/vim-airline'
Plugin 'rakr/vim-one'
Plugin 'ycm-core/YouCompleteMe'
Plugin 'preservim/nerdtree'
Plugin 'Xuyuanp/nerdtree-git-plugin'
Plugin 'ryanoasis/vim-devicons'
Plugin 'Yggdroot/indentLine'
Plugin 'sainnhe/sonokai'

" ending vundle config
call vundle#end()
filetype plugin indent on
"---End Vundle config---"

" My options
set nu
set mouse=a
" If you have a crappy terminal (emulator) then vim will try to use the closest possible
" color values, or else it wil use the true color
if (empty($TMUX))
    if (has("nvim"))
        let $NVIM_TUI_ENABLE_TRUE_COLOR=1
    endif
    if (has("termguicolors"))
        set termguicolors
    endif
endif
let g:sonaki_style='atlantis'
colorscheme sonokai
set noshowmode " do not show the mode, it is shown by airline
set showcmd " show current command
set expandtab
set shiftwidth=4
set softtabstop=4
syntax on
let g:vim_json_conceal = 0

set showtabline=2 " makes the tabline visible
" makes comments italic
highlight Comment cterm=italic

" nerdtree stuff
let g:NERDTreeGitStatusUseNerdFonts = 1

let &t_ut=''
set hidden
inoremap jj <Esc>
